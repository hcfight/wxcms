<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">


    <title>后台管理系统</title>
    <% String path = request.getContextPath(); %>
    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css" media="all">

</head>
<body>
<div class="container">

    <form id="linkForm" class="layui-form" method="post" action="" enctype="multipart/form-data">
           <input id="link_pk" name="link_pk" type="hidden" />
           <input id="link_images" name="link_images" type="hidden">
           <div class="layui-form-item" style="margin: 5% auto">
               <label class="layui-form-label">标题</label>
               <div class="layui-input-block">
                   <input id="link_title" name="link_title"  autocomplete="off" placeholder="请输入标题" class="layui-input" type="text"/>
               </div>
           </div>

           <div class="layui-form-item">
               <label class="layui-form-label">路径</label>
               <div class="layui-input-block">
                   <input id="link_address" name="link_address" lay-verify="required" autocomplete="off" placeholder="请输入路径" class="layui-input" type="text">
               </div>
           </div>

        <div class="layui-form-item">
            <label class="layui-form-label">图片</label>
            <div class="site-demo-upload">
                <img id="images" src="" width="150px" height="50px" title="">
            </div>
        </div>


       <div class="layui-form-item">
           <label class="layui-form-label">图片</label>
           <div class="layui-input-block">
               <button type="button" class="layui-btn" id="file">
                   <i class="layui-icon">&#xe67c;</i>上传图片
               </button>
           </div>
       </div>

        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input id="link_remarks" name="link_remarks" lay-verify="required" autocomplete="off" class="layui-input" type="text">
            </div>
        </div>


           <div class="layui-form-item">
               <div align="center" class="layui-input-block" style="margin: 5% auto">
                   <button class="layui-btn layui-btn-small" align="center" id="edit1" onclick="edit()">保存</button>
                   <button class="layui-btn layui-btn-small" id="reset" align="center" type="reset">重置</button>
               </div>
           </div>

    </form>
</div>
</body>


<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<%--layui--%>
<script>


    layui.use(['form','laydate','upload'],function () {
        var $ = layui.jquery;
        var form = layui.form;
        var layer = layui.layer;
        var upload = layui.upload; // 获取upload模块


        $(function (){
            debugger;
            var link_pk=$("#link_pk").val();
            if(link_pk!=null && link_pk!=''){
               $.ajax({
                   type:'post',
                   url:"<%=path%>/link/list",
                   data:{link_pk:link_pk},
                   success:function (response) {
                       var json = eval(response); // 数组
                       $.each(json,function (i) {
                           $("#link_pk").val(json[i].link_pk);
                           $("#link_title").val(json[i].link_title);
                           $("#link_address").val(json[i].link_address);
                           $("#link_remarks").val(json[i].link_remarks);
                           $("#images").attr("src",json[i].link_images);
                           $("#images").attr("title",json[i].link_images);
                           $("#link_images").val(json[i].link_images);
                       })
                       form.render('radio');
                       form.render('select');
                   }
               });
            }
        });


        //执行实例
        var uploadInst = upload.render({
            elem: '#file' //绑定元素
            ,url: '<%=path%>/file/upload' //上传接口
            ,done: function(res){
                //上传完毕回调
                if (res.state=="success")
                {
                    $("#images").attr("src",res.msg);
                    $("#link_images").val(res.msg)
                }
                else {
                    parent.layer.alert("上传失败");
                    return false;
                }
            }
            ,error: function(){
                //请求异常回调
            }
        });


        edit=function () {
            debugger;
            var link_pk=$("#link_pk").val();
            var link_address=$("#link_address").val();
            var url="<%=path%>/link/saveOrUpdate";
            // $("#linkForm").attr("action",url).submit();
            if(link_address!=null && link_address!=''){
                $.ajax({
                    type : 'post',
                    async : false,//同步请求，执行成功后才会继续执行后面的代码
                    url : url,
                    data : {
                        link_pk:link_pk,
                        link_title : $("#link_title").val(),
                        link_address : $("#link_address").val(),
                        link_remarks : $("#link_remarks").val(),
                        link_images : $("#link_images").val(),
                    },
                    success : function (response) {
                            parent.layer.alert(response.msg);
                            window.location.reload();
                    },
                    error : function (response) {
                        parent.layer.alert(response.msg);
                        window.location.reload();
                    }
                });
                return false;
            }

        }

    })

</script>
</html>
