package com.weixun.utils.page;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * Base Framework - 后台分页封装类.
 * <p><br>
 * @author 马玉德
 * @param <T> 实体
 * @version 1.0.0, 2013-9-4
 */
public class DataPage<T> implements Serializable {
	/**
	 * 序列化标识
	 */
	private static final long serialVersionUID = -1258609270248897643L;
	/**
	 * 当前页页数
	 */
	private int page ;
	/**
	 * 总页数
	 */
	private int total;
	/**
	 * 总记录数
	 */
	private long records;
	/**
	 * 分页实体集合
	 */
	private List rows;
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public long getRecords() {
		return records;
	}

	public void setRecords(long records) {
		this.records = records;
	}

	public List getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}	
}
