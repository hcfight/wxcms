package com.weixun.admin.controller;

import com.jfinal.core.Controller;
import com.weixun.admin.service.StaffService;
import com.weixun.admin.shiro.ShiroCache;
import com.weixun.utils.ajax.AjaxMsg;
import com.weixun.utils.md5.Md5Utils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

/**
 * Created by myd on 2016/12/31.
 */
public class MainController extends Controller {

    StaffService staffService = new StaffService();
    /**
     * 生成验证码
     */
    public void code()
    {
        try {
//            verifyCode = new VerifyCode();
//            verifyCode.getCode(req, resp);
            //jfina 自带的验证码
            renderCaptcha();

        }catch (Exception e)
        {
            e.getStackTrace();
        }
    }
    /**
     * 登录成功后跳转到主界面
     */
    public void index()
    {
        this.render("/login.jsp");
//        this.render("/views/index.jsp");
    }

    /**
     * 退出登录
     */
    public void logout()
    {
        ShiroCache.userLogout();
        this.render("/login.jsp");
    }
    /**
     * 登录成功后跳转页面
     */
    public void loginSuccess()
    {
        if (getSessionAttr("loginUser")!=null) {
            this.render("/views/index.jsp");
        }
        else
        {
            this.render("/login.jsp");
        }
    }

    public void login()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        String staff_loginname = getPara("staff_loginname");
        String staff_password  = getPara("staff_password");
        //imgcode为参数name
        boolean result = validateCaptcha("code_value");
        if(result){
            try {
                Subject us = SecurityUtils.getSubject();
                //检查是否登录
                if (!us.isAuthenticated()) {

                    // 未登录则重新验证
                   String res = shiroLogin(staff_loginname, staff_password);
                   if (res.equals("success"))
                   {
                       ajaxMsg.setState("success");
                       ajaxMsg.setMsg("登录成功！");
                   }
                   else
                   {
                       ajaxMsg.setState("fail");
                       ajaxMsg.setMsg(res);
                   }
                }

            }
            catch (Exception e)
            {
                e.getMessage();
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("登录失败！");
            }
        }else
        {
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("验证码错误！");
        }
        renderJson(ajaxMsg);
    }






    private String shiroLogin(String username,String passwd) {
        try {
            UsernamePasswordToken token = new UsernamePasswordToken(username, Md5Utils.ToMd5(passwd,1));
            token.setRememberMe(true);
            Subject currentUser = SecurityUtils.getSubject();
            currentUser.login(token);
        } catch (UnknownAccountException ex) {
            return "账号不存在!";
        } catch (AuthenticationException ex) {
            return "密码错误!";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "初始化失败，请重试!";
        }
        return "success";
    }


}
