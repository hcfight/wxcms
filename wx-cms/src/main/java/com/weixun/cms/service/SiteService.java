package com.weixun.cms.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public class SiteService {
    /**
     * 分页
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Record> paginate(int pageNumber, int pageSize,String site_name) {

        StringBuffer  sqlstr = new StringBuffer();
        sqlstr.append("from sys_site s  where 1=1 ");
        if (site_name != null && !site_name.equals("")) {
            sqlstr.append("and s.site_name like '%" + site_name + "%' ");
        }
        sqlstr.append(" order by s.site_pk desc ");

        String select = "select *";

        return Db.paginate(pageNumber, pageSize, select, sqlstr.toString());

//        return Db.paginate(pageNumber, pageSize, "select *", "from sys_site order by site_pk desc");
    }


    /**
     * 获取列表
     * @return
     */
    public List<Record> findList(String site_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from sys_site where 1=1 ");
        if (site_pk != null && !site_pk.equals(""))
        {
            stringBuffer.append("and site_pk = "+site_pk);
        }
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        return list;
    }


    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from sys_site where site_pk in ("+ids+")";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }
}
